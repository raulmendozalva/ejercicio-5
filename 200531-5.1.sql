SELECT O.OrdenesNumero, C.ClientesNombre, E.EmpleadosNombre 
FROM Ordenes O, Clientes C, Empleados E
WHERE O.ClientesCodigo = C.ClientesCodigo
and O.EmpleadosCodigo = E.EmpleadosCodigo
ORDER BY C.ClientesNombre, O.OrdenesNumero
--ORDER BY 2,1


--SELECT O.OrdenesNumero, C.ClientesNombre, E.EmpleadosNombre 
--FROM Ordenes O
--INNER JOIN Clientes C ON O.ClientesCodigo = C.ClientesCodigo
--INNER JOIN Empleados E ON O.EmpleadosCodigo =  E.EmpleadosCodigo
--ORDER BY C.ClientesNombre, O.OrdenesNumero
----ORDER BY 2,1

