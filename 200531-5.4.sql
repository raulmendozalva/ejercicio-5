DECLARE @initDate datetime, @endDate datetime
SET @initDate = '2020-01-01'
SET @endDate = '2020-03-31'

SET @endDate = DATEADD(SECOND,-1,DATEADD(DAY,1,@endDate))

SELECT COUNT(O.OrdenesNumero) AS [Total de �rdenes]
FROM Productos P, DetalleOrdenes D, Ordenes O, Clientes C, Empleados E
WHERE P.ProductosCodigo = D.ProductosCodigo
AND D.OrdenesNumero = O.OrdenesNumero
AND O.ClientesCodigo = C.ClientesCodigo
AND O.EmpleadosCodigo = E.EmpleadosCodigo
AND P.ProductosDescripcion IN ('Leche', 'Harina', 'Huevo')
AND O.OrdenesFecha BETWEEN @initDate AND @endDate
AND C.ClientesNombre in ('Juan Perez', 'Pedro Hernandez')
AND E.EmpleadosCodigo like '%A01%'
GROUP BY O.OrdenesNumero